Source: libkdegames
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Daniel Schepler <schepler@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Modestas Vainius <modax@debian.org>,
           George Kiagiadakis <gkiagiad@csd.uoc.gr>,
           Eshat Cakar <info@eshat.de>,
           Maximiliano Curia <maxy@debian.org>,
Build-Depends: cmake (>= 3.5.0~),
               debhelper-compat (= 12),
               extra-cmake-modules (>= 5.46.0~),
               gettext,
               libkf5archive-dev (>= 5.46.0~),
               libkf5bookmarks-dev (>= 5.46.0~),
               libkf5codecs-dev (>= 5.46.0~),
               libkf5completion-dev (>= 5.46.0~),
               libkf5config-dev (>= 5.46.0~),
               libkf5configwidgets-dev (>= 5.46.0~),
               libkf5coreaddons-dev (>= 5.46.0~),
               libkf5crash-dev (>= 5.46.0~),
               libkf5dbusaddons-dev (>= 5.46.0~),
               libkf5declarative-dev (>= 5.46.0~),
               libkf5dnssd-dev (>= 5.46.0~),
               libkf5globalaccel-dev (>= 5.46.0~),
               libkf5guiaddons-dev (>= 5.46.0~),
               libkf5i18n-dev (>= 5.46.0~),
               libkf5iconthemes-dev (>= 5.46.0~),
               libkf5itemviews-dev (>= 5.46.0~),
               libkf5jobwidgets-dev (>= 5.46.0~),
               libkf5newstuff-dev (>= 5.46.0~),
               libkf5service-dev (>= 5.46.0~),
               libkf5textwidgets-dev (>= 5.46.0~),
               libkf5widgetsaddons-dev (>= 5.46.0~),
               libkf5xmlgui-dev (>= 5.46.0~),
               libopenal-dev,
               libqt5svg5-dev (>= 5.9.0~),
               libsndfile1-dev,
               pkg-kde-tools (>= 0.14),
               qtbase5-dev (>= 5.9.0~),
               qtdeclarative5-dev (>= 5.9.0~),
               xauth,
               xvfb,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/libkdegames
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/libkdegames.git

Package: kdegames-card-data-kf5
Architecture: all
Section: games
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: card decks for KDE games
 This package contains a collection of playing card themes for KDE card games.
 .
 This package is part of the KDE games module.

Package: libkf5kdegames-dev
Architecture: any
Section: libdevel
Depends: libkf5completion-dev,
         libkf5config-dev,
         libkf5configwidgets-dev,
         libkf5i18n-dev,
         libkf5widgetsaddons-dev,
         libkf5kdegames7 (= ${binary:Version}),
         libkf5kdegamesprivate1 (= ${binary:Version}),
         qtbase5-dev (>= 5.9.0~),
         qtdeclarative5-dev (>= 5.9.0~),
         ${misc:Depends},
Description: development files for the KDE games library
 This package contains development files for building software that uses
 libraries from the KDE games module.
 .
 This package is part of the KDE games module.

Package: libkf5kdegames7
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: ${kde-l10n:all}
Replaces: ${kde-l10n:all}
Description: shared library for KDE games
 This package contains a shared library used by KDE games.
 .
 This package is part of the KDE games module.

Package: libkf5kdegamesprivate1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: private part of shared library for KDE games
 This package contains the private parts of the shared library used by KDE
 games.
 .
 This package is part of the KDE games module.

Package: qml-module-org-kde-games-core
Architecture: any
Depends: qml-module-qtquick2, ${misc:Depends}, ${shlibs:Depends}
Description: kde-games-core QtDeclarative QML support
 Contains a plugin for QtDeclarative that provides
 support for using KDE-games-core to components written
 in QML.
 .
 This package is part of the KDE games module.
